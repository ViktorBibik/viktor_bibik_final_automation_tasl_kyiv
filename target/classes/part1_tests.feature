Feature: Smoke
  As a user
  I want to test all main site functionality
  So that I can be sure that site works correctly

  Scenario Outline: 1. Positive - Get first abstract title text.
    Given User opens '<homePage>' page
    When User clicks News tab
    Then User checks that Header Text From First Abstract is not empty

    Examples:
      | homePage             | news_title                                         |
      | https://www.bbc.com/ | Tonga undersea cable may take four weeks to repair |


  Scenario Outline: 2. Positive - Get Headers List Size.
    Given User opens '<homePage>' page
    When User clicks News tab
    Then User checks that Header List size from top News block equals at least <header_list>

    Examples:
      | homePage             | header_list |
      | https://www.bbc.com/ | 8           |


  Scenario Outline: 3. Positive - Check the name of the first article against a specified value.
    Given User opens '<homePage>' page
    When User clicks News tab
    And User sends first article category name to search field
    Then User checks that first article title equals to '<category_title>'

    Examples:
      | homePage             | category_title      |
      | https://www.bbc.com/ | At The Edge of Asia |