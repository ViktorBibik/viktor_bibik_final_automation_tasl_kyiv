# Task 2

### Where we are

We have a few basic Task1. Let’s add a few more, with more complicated logic and Xpath.

### Task

Implement Task1 described in Part2 file of your variant.

### Result

We have a bunch of very similar Task1. As you can imagine, writing Task1 in this manner will very quickly make them impossible to maintain (any small change will force us to modify many Task1). In the next part, we will start working on reducing the duplication.
