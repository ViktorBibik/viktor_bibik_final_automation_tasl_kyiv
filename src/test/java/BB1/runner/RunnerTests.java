package BB1.runner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        features =
                {"src/main/resources/part1_tests.feature"
                        , "src/main/resources/part2_tests.feature"
                },
        glue = "BB1.stepdefinitions"
)
public class RunnerTests extends AbstractTestNGCucumberTests {
}
