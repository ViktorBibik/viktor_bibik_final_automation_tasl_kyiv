package BB1.Part2;

import BB1.BaseTest;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

public class Part2_tests extends BaseTest {

    private final String ERROR_MESSAGE_TEXT = "There’s an error to correct before you can continue";

    @Test(priority = 1)
    public void sendQuestionToBBCWithoutTitle(){
        assertEquals(getBusinessLogicLayer().getErrorMessageWhenRequestWithoutTitle(), ERROR_MESSAGE_TEXT);
    }

    @Test(priority = 2)
    public void sendQuestionToBBCWithoutTextBox(){
        assertEquals(getBusinessLogicLayer().getErrorMessageWhenRequestWithoutTextBox(), ERROR_MESSAGE_TEXT);
    }

    @Test(priority = 3)
    public void sendQuestionToBBCWithoutTextAnyText(){
        assertEquals(getBusinessLogicLayer().getErrorMessageWhenRequestWithoutText(), ERROR_MESSAGE_TEXT);
    }
}

