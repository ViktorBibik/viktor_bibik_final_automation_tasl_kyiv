package BB1.Part1;


import BB1.BaseTest;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

public class Part1_tests extends BaseTest {

    private static final int TITLES_ARRAY_LENGTH = 10;
    private static final String CATEGORY_TITLE = "At The Edge of Asia";

    @Test
    public void checkFirstAbstractTitle(){
        assertEquals(getBusinessLogicLayer().isHeaderTextFromFirstAbstractEmpty(), false);
    }

    @Test
    public void checkTitlesListPresence(){
        assertTrue(getBusinessLogicLayer().getTitlesListElementsCount()>= TITLES_ARRAY_LENGTH);
    }

    @Test
    public void getCategoryText(){
//        assertEquals(getBusinessLogicLayer().checkSearchFieldFromNewsPage(), CATEGORY_TITLE);
        assertEquals(getBusinessLogicLayer().checkSearchFieldFromNewsPage(), CATEGORY_TITLE);
    }
}
