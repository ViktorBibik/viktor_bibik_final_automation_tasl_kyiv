package BB1.BLL;

import BB1.managers.PageFactoryManager;
import org.openqa.selenium.WebDriver;

import static BB1.helpers.WebHelpers.scrollPageToCurrentElement;

public class BusinessLogicLayer extends PageFactoryManager {
    private static final String BBC_URL = "https://www.bbc.com/";

    private final String enquireFieldText = "Some text.";

    public BusinessLogicLayer(WebDriver driver) {
        super(driver);
    }

    public String getHeaderTextFromFirstAbstractOnTheNewsPage(){
        getHomePage().clickNewsTab();
        return getNewsPage().getFirstNewsBlockTitleFromTopStories();
    }

    public boolean isHeaderTextFromFirstAbstractEmpty(){
        getHomePage().clickNewsTab();
        return getNewsPage().isHeaderTextNotEmpty();
    }

    public String[] getTitlesList(){
        getHomePage().clickNewsTab();
        return getNewsPage().getAllTitlesHeadersTextInMainNewsBlock();
    }

    public int getTitlesListElementsCount(){
        getHomePage().clickNewsTab();
        return getNewsPage().getHeadersListSize();
    }

    public String checkSearchFieldFromNewsPage(){
        getHomePage().clickNewsTab();
        getHomePage().sendTextToSearchField(getNewsPage().getCategoryTextFromFirstNewsBlock());
        return getSearchPage().getFirstRegionalNewsTitle();
    }

    public String getErrorMessageWhenRequestWithoutTitle(){
        sendQuestionToBBCWithoutTitle();
        return getMainErrorMessage();
    }

    public String getErrorMessageWhenRequestWithoutTextBox(){
        sendQuestionToBBCWithoutTextBox();
        return getMainErrorMessage();
    }

    public String getErrorMessageWhenRequestWithoutText(){
        sendQuestionToBBCWithoutText();
        return getMainErrorMessage();
    }

    private void sendQuestionToBBCWithoutText(){
        getHomePage().waitClickableOfElement(5, getHomePage().getContactTheBBCLink());
        scrollPageToCurrentElement(getHomePage().getContactTheBBCLink(), getDriver());
        getHomePage().clickContactTheBBCLink();
        getContactTheBBCPage().clickQuestionTab();
        getContactTheBBCPage().clickHowCanIContactLink();
        getContactTheBBCPage().clickGetMoreHelpButton();
        getMoreHelpQuestionTab();
        getContactTheBBCPage().clickContinueButton();
    }

    private void sendQuestionToBBCWithoutTextBox(){
        getHomePage().waitClickableOfElement(5, getHomePage().getContactTheBBCLink());
        scrollPageToCurrentElement(getHomePage().getContactTheBBCLink(), getDriver());
        getHomePage().clickContactTheBBCLink();
        getContactTheBBCPage().clickQuestionTab();
        getContactTheBBCPage().clickHowCanIContactLink();
        getContactTheBBCPage().clickGetMoreHelpButton();
        getMoreHelpQuestionTab();
        sendTextToEnquiryTitleInputFieldBox();
    }

    private void sendQuestionToBBCWithoutTitle(){
        getHomePage().waitClickableOfElement(5, getHomePage().getContactTheBBCLink());
        scrollPageToCurrentElement(getHomePage().getContactTheBBCLink(), getDriver());
        getHomePage().clickContactTheBBCLink();
        getContactTheBBCPage().clickQuestionTab();
        getContactTheBBCPage().clickHowCanIContactLink();
        getContactTheBBCPage().clickGetMoreHelpButton();
        getMoreHelpQuestionTab();
        sendTextToEnquiryTextarea();
    }

    private void getMoreHelpQuestionTab(){
        getContactTheBBCPage().clickIWantToList();
        getContactTheBBCPage().clickIWantInformationAbout();
        getContactTheBBCPage().clickContinueButton();
    }

    private void sendTextToEnquiryTextarea(){
        getContactTheBBCPage().sendTextToEnquiryTextarea(enquireFieldText);
        getContactTheBBCPage().clickContinueButtonGetMoreHelpForm();
    }

    private void sendTextToEnquiryTitleInputFieldBox(){
        getContactTheBBCPage().sendTextToEnquiryTitleInputField(enquireFieldText);
        getContactTheBBCPage().clickContinueButtonGetMoreHelpForm();
    }

    private String getMainErrorMessage(){
        return getContactTheBBCPage().getErrorMessage().getText();
    }
}
