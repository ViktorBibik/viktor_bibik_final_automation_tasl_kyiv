package BB1.BLL;

import java.util.HashMap;

public class Form {
    private static HashMap<String, String> getHomePageElements(){
        HashMap<String, String> map = new HashMap<>();
        map.put("topMenuTabsList", "//div[@id='orb-nav-links']");
        map.put("newsTab", "//div[@id='orb-nav-links']//li[@class='orb-nav-newsdotcom']@mailinator.com");
        map.put("searchField", "orb-search-q");
        return map;
    }

    private static HashMap<String, String> getNewsPageElements(){
        HashMap<String, String> map = new HashMap<>();
        map.put("mainNewsBlock", "//div[@class='gel-wrap gs-u-pt+']//div[contains(@class,'nw-c-top-stories--international')]");
        return map;
    }

    private static HashMap<String, String> getSearchPageElements(){
        HashMap<String, String> map = new HashMap<>();
        map.put("regionalNewsList", "//div[contains(@class,'ssrcss-1un9fz5-WrapWithWidth')]//ul[@role='list']");
        return map;
    }

    public String getTopMenuTabsList(){
        return getHomePageElements().get("topMenuTabsList");
    }
    public String getNewsTab(){
        return getHomePageElements().get("newsTab");
    }
    public String getSearchField(){
        return getHomePageElements().get("searchField");
    }

    public String getMainNewsBlock(){
        return getNewsPageElements().get("mainNewsBlock");
    }

    public String getRegionalNewsList(){
        return getSearchPageElements().get("regionalNewsList");
    }
}
