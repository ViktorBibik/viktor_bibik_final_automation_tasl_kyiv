package BB1.helpers;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.List;

public class WebHelpers {
    public static void clickWebElementFromListByText(WebElement element, String text, String tagName) {
        List<WebElement> listSpanElements = element.findElements(By.tagName(tagName));

        for (WebElement elRequired : listSpanElements) {
            if (elRequired.getText().contains(text)) {
                elRequired.click();
                break;
            }
        }
    }

    public static int countWebElementsInList(WebElement element, String tagName) {
        List<WebElement> listSpanElements = element.findElements(By.tagName(tagName));
        int count = 0;
        for (WebElement e : listSpanElements) {
            if (e.isEnabled()) count++;
            else throw new ArithmeticException();
        }
        return count;
    }

    public static void scrollPageToCurrentElement(WebElement element, WebDriver driver){
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView();", element);
    }


    public static void moveCursorToCurrentWebElement(WebElement element, WebDriver driver){
        Actions actions = new Actions(driver);
        actions.moveToElement(element).perform();
    }
}
