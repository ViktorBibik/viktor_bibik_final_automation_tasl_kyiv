package BB1.managers;

import BB1.pages.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class PageFactoryManager {
    WebDriver driver;

    public PageFactoryManager(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public WebDriver getDriver(){
        return driver;
    }

    public HomePage getHomePage(){
        return new HomePage(getDriver());
    }

    public NewsPage getNewsPage(){
        return new NewsPage(getDriver());
    }

    public SearchPage getSearchPage(){
        return new SearchPage(getDriver());
    }

    public ContactTheBBCPage getContactTheBBCPage(){
        return new ContactTheBBCPage(getDriver());
    }
}
