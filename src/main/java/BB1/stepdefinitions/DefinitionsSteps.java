package BB1.stepdefinitions;

import BB1.managers.PageFactoryManager;
import BB1.pages.ContactTheBBCPage;
import BB1.pages.HomePage;
import BB1.pages.NewsPage;
import BB1.pages.SearchPage;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

import static BB1.helpers.WebHelpers.scrollPageToCurrentElement;
import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;

public class DefinitionsSteps {
    WebDriver driver;
    PageFactoryManager pageFactoryManager;
    HomePage homePage;
    NewsPage newsPage;
    SearchPage searchPage;
    ContactTheBBCPage contactTheBBCPage;

    @Before
    public void testsSetUp() {
        chromedriver().setup();
        driver = new ChromeDriver();
        pageFactoryManager = new PageFactoryManager(driver);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @After
    public void tearDown() {
        driver.close();
    }


    @And("User opens {string} page")
    public void userOpensTheHomePagePage(final String url) {
        homePage = pageFactoryManager.getHomePage();
        homePage.openHomePage(url);
    }

    @And("User clicks News tab")
    public void userClicksNewsTab() {
        homePage.clickNewsTab();
    }

    @And("User checks that Header Text From First Abstract is not empty")
    public void userChecksThatHeaderTextFromFirstAbstractOnTheNewsPageEqualsToNews_title() {
        newsPage = pageFactoryManager.getNewsPage();
        Assert.assertTrue(newsPage.getHeaderTextLength() >= 1);
    }

    @Then("User checks that Header List size from top News block equals at least {int}")
    public void userChecksThatHeaderListSizeFromTopNewsBlockEqualsAtLeastHeader_list(final int abstract_count) {
        newsPage = pageFactoryManager.getNewsPage();
        Assert.assertTrue(newsPage.getHeadersListSize() >= abstract_count);
    }

    @And("User sends first article category name to search field")
    public void userSendsFirstArticleCategoryNameToSearchField() {
        homePage = pageFactoryManager.getHomePage();
        newsPage = pageFactoryManager.getNewsPage();
        homePage.sendTextToSearchField(newsPage.getCategoryTextFromFirstNewsBlock());
    }

    @Then("User checks that first article title equals to {string}")
    public void userChecksThatFirstArticleTitleEqualsToCategory_title(final String category_title) {
        searchPage = pageFactoryManager.getSearchPage();
        Assert.assertEquals(searchPage.getFirstRegionalNewsTitle(), category_title);
    }

    @And("User clicks Contact Us button into page footer")
    public void userClicksContactUsButtonIntoPageFooter() {
        homePage.waitClickableOfElement(5, homePage.getContactTheBBCLink());
        scrollPageToCurrentElement(homePage.getContactTheBBCLink(), driver);
        homePage.clickContactTheBBCLink();
    }

    @And("User clicks Question Tab")
    public void userClicksQuestionTab() {
        contactTheBBCPage = pageFactoryManager.getContactTheBBCPage();
        contactTheBBCPage.clickQuestionTab();

    }

    @And("User clicks How Can I Contact Link")
    public void userClicksHowCanIContactLink() {
        contactTheBBCPage.clickHowCanIContactLink();
    }

    @And("User clicks Get More Help Button")
    public void userClicksGetMoreHelpButton() {
        contactTheBBCPage.clickGetMoreHelpButton();
    }

    @And("User clicks I Want To List variant in list")
    public void userClicksIWantToListVariantInList() {
        contactTheBBCPage.clickIWantToList();
    }

    @And("User clicks I Want Information About in list")
    public void userClicksIWantInformationAboutInList() {
        contactTheBBCPage.clickIWantInformationAbout();
    }

    @And("User clicks Continue Button")
    public void userClicksContinueButton() {
        contactTheBBCPage.clickContinueButton();
    }

    @When("User sends Text {string} To Enquiry Text area")
    public void userSendsTextToEnquiryTextArea(final String TEXT) {
        contactTheBBCPage.sendTextToEnquiryTextarea(TEXT);
    }

    @When("User clicks button Continue in Get More Help Form")
    public void userSendsClicksButtonContinueInGetMoreHelpForm() {
        contactTheBBCPage.clickContinueButtonGetMoreHelpForm();
    }

    @Then("The error message {string} should to be appeared")
    public void theErrorMessageError_messageShouldToBeAppeared(final String errorMessageText) {
        Assert.assertEquals(contactTheBBCPage.getErrorMessage().getText(), errorMessageText);
    }

    @When("User sends Text {string} To Enquiry Title Input Field")
    public void userSendsTextRequired_textToEnquiryTitleInputField(final String TEXT) {
        contactTheBBCPage.sendTextToEnquiryTitleInputField(TEXT);
    }
}
